package arquivoDeNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroTipo3 struct {
	Identificador                    string  `json:"Identificador"`
	ValorTotalProdutos               float64 `json:"ValorTotalProdutos"`
	ValorIPI                         float64 `json:"ValorIPI"`
	ValorFrete                       float64 `json:"ValorFrete"`
	ValorSeguro                      float64 `json:"ValorSeguro"`
	ValorOutrasDespesas              float64 `json:"ValorOutrasDespesas"`
	ValorDescontoDadoTotalNotaFiscal float64 `json:"ValorDescontoDadoTotalNotaFiscal"`
	ValorRepasseICMS                 float64 `json:"ValorRepasseICMS"`
	ValorTotalNotaFiscal             float64 `json:"ValorTotalNotaFiscal"`
	ValorSubstituicaoTributaria      float64 `json:"ValorSubstituicaoTributaria"`
}

func (r *RegistroTipo3) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroTipo3

	err = posicaoParaValor.ReturnByType(&r.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorTotalProdutos, "ValorTotalProdutos")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorIPI, "ValorIPI")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorFrete, "ValorFrete")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorSeguro, "ValorSeguro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorOutrasDespesas, "ValorOutrasDespesas")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorDescontoDadoTotalNotaFiscal, "ValorDescontoDadoTotalNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorRepasseICMS, "ValorRepasseICMS")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorTotalNotaFiscal, "ValorTotalNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorSubstituicaoTributaria, "ValorSubstituicaoTributaria")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistroTipo3 = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":                    {0, 2, 0},
	"ValorTotalProdutos":               {2, 11, 2},
	"ValorIPI":                         {11, 20, 2},
	"ValorFrete":                       {20, 29, 2},
	"ValorSeguro":                      {29, 38, 2},
	"ValorOutrasDespesas":              {38, 47, 2},
	"ValorDescontoDadoTotalNotaFiscal": {47, 56, 2},
	"ValorRepasseICMS":                 {56, 65, 2},
	"ValorTotalNotaFiscal":             {65, 74, 2},
	"ValorSubstituicaoTributaria":      {74, 83, 2},
}
