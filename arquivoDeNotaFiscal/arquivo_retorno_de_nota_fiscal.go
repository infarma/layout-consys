package arquivoDeNotaFiscal

import (
	"fmt"
	"time"
)

func Registro01(cnpjFornecedor string, inscEstadualFornecedor  string, siglaUniFederacaoFornecedor string,
	codPedidoCliente string, dataEmissao time.Time, dataSaidaNota time.Time, cnpjCliente string) string {

	registro01 := fmt.Sprint("01")
	registro01 += fmt.Sprintf("%014s", cnpjFornecedor)
	registro01 += fmt.Sprintf("%-14s", inscEstadualFornecedor)
	registro01 += fmt.Sprintf("%02s", siglaUniFederacaoFornecedor)
	registro01 += fmt.Sprintf("%-10s", codPedidoCliente)
	registro01 +=  fmt.Sprint(dataEmissao.Format("02012006150405"))
	registro01 +=  fmt.Sprint(dataSaidaNota.Format("02012006"))
	registro01 += fmt.Sprintf("%014s", cnpjCliente)

	return registro01
}

func Registro02(numNotaFiscal int64, modeloDaNotaFiscal string, serieNotaFiscal string, baseICMSSubTri float64, baseICMS float64) string {

	registro02 := fmt.Sprint("02")
	registro02 += fmt.Sprintf("%06d", numNotaFiscal)
	registro02 += fmt.Sprintf("%-3s", modeloDaNotaFiscal)
	registro02 += fmt.Sprintf("%-5s", serieNotaFiscal)
	registro02 += fmt.Sprintf("%09d", int64(baseICMSSubTri*100))
	registro02 += fmt.Sprintf("%09d", int64(baseICMS*100))

	return registro02
}

func Registro03(valorTotalProdutos float64, valorIPI float64, valorFrete float64, valorSeguro float64, valorDespesas float64,
								valorDescontoTotal float64, valorRepasseICMS float64, valorTotalNotaFiscal float64, valorSubsTribuICMS float64) string {

	registro03 := fmt.Sprint("03")
	registro03 += fmt.Sprintf("%09d", int64(valorTotalProdutos*100))
	registro03 += fmt.Sprintf("%09d", int64(valorIPI*100))
	registro03 += fmt.Sprintf("%09d", int64(valorFrete*100))
	registro03 += fmt.Sprintf("%09d", int64(valorSeguro*100))
	registro03 += fmt.Sprintf("%09d", int64(valorDespesas*100))
	registro03 += fmt.Sprintf("%09d", int64(valorDescontoTotal*100))
	registro03 += fmt.Sprintf("%09d", int64(valorRepasseICMS*100))
	registro03 += fmt.Sprintf("%09d", int64(valorTotalNotaFiscal*100))
	registro03 += fmt.Sprintf("%09d", int64(valorSubsTribuICMS*100))

	return registro03
}

func Registro04(codProdutoForn int64, codBarrasProduto int64, qtdFatura int32, valorUnitario float64, valorDesconto float64,
								baseCalculoICMS float64, baseICMSSubTri float64, percentualICMS float32, percentualIPI float32,
								valorICMS float64, valorIPI float64, valorICMSRepassado float64, cFOP int32, codSituacaoTributaria int8,
								ideListaPositiva string, numOrdemImpressaoNotaFiscal int32, prazoPorProduto int32, valorRepasse float64,
								valorTotalProduto float64, cSTPorduto int32, valorUniDescontoFinanceiro float64) string {

	registro04 := fmt.Sprint("04")
	registro04 += fmt.Sprintf("%010d", codProdutoForn)
	registro04 += fmt.Sprintf("%013d", codBarrasProduto)
	registro04 += fmt.Sprintf("%05d", qtdFatura)
	registro04 += fmt.Sprintf("%09d", int64(valorUnitario*100))
	registro04 += fmt.Sprintf("%09d", int64(valorDesconto*100))
	registro04 += fmt.Sprintf("%09d", int64(baseCalculoICMS*100))
	registro04 += fmt.Sprintf("%09d", int64(baseICMSSubTri*100))
	registro04 += fmt.Sprintf("%04d", int64(percentualICMS*100))
	registro04 += fmt.Sprintf("%04d", int64(percentualIPI*100))
	registro04 += fmt.Sprintf("%09d", int64(valorICMS*100))
	registro04 += fmt.Sprintf("%09d", int64(valorIPI*100))
	registro04 += fmt.Sprintf("%09d", int64(valorICMSRepassado*100))
	registro04 += fmt.Sprintf("%04d", cFOP)
	registro04 += fmt.Sprintf("%01d", codSituacaoTributaria)
	registro04 += fmt.Sprintf("%01s", ideListaPositiva)
	registro04 += fmt.Sprintf("%04d", numOrdemImpressaoNotaFiscal)
	registro04 += fmt.Sprintf("%03d", prazoPorProduto)
	registro04 += fmt.Sprintf("%09d", int64(valorRepasse*100))
	registro04 += fmt.Sprintf("%09d", int64(valorTotalProduto*100))
	registro04 += fmt.Sprintf("%03d", cSTPorduto)
	registro04 += fmt.Sprintf("%09d", int64(valorUniDescontoFinanceiro*100))

	return registro04
}

func Registro05(cFOP int32, aliquotaICMS float32, baseCalculoICMS float64, valorICMS float64, valorIsentoNaoTributado float64,
								valorNaoConfiraDebitoCreditoICMS float64) string {

	registro05 := fmt.Sprint("05")
	registro05 += fmt.Sprintf("%04d", cFOP)
	registro05 += fmt.Sprintf("%04d", int64(aliquotaICMS*100))
	registro05 += fmt.Sprintf("%09d", int64(baseCalculoICMS*100))
	registro05 += fmt.Sprintf("%09d", int64(valorICMS*100))
	registro05 += fmt.Sprintf("%09d", int64(valorIsentoNaoTributado*100))
	registro05 += fmt.Sprintf("%09d", int64(valorNaoConfiraDebitoCreditoICMS*100))

	return registro05
}

func Registro06(valorTotalNota float64, valorTotalProduto float64) string {

	registro06 := fmt.Sprint("06")
	registro06 += fmt.Sprintf("%09d", int64(valorTotalNota*100))
	registro06 += fmt.Sprintf("%09d", int64(valorTotalProduto*100))

	return registro06
}

func Registro08(numIdentificacaoBloqueto string, dataEmissao time.Time, dataVencimento time.Time, valorParcela float64,
								numNotaFiscal int64) string {

	registro08 := fmt.Sprint("08")
	registro08 += fmt.Sprintf("%-30s", numIdentificacaoBloqueto)
	registro08 += fmt.Sprint(dataEmissao.Format("02012006"))
	registro08 += fmt.Sprint(dataVencimento.Format("02012006"))
	registro08 += fmt.Sprintf("%09d", int64(valorParcela*100))
	registro08 += fmt.Sprintf("%06d", numNotaFiscal)

	return registro08
}