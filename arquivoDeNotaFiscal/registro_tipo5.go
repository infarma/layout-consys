package arquivoDeNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroTipo5 struct {
	Identificador                    string  `json:"Identificador"`
	CFOP                             int32   `json:"CFOP"`
	PercentualAliquotaICMS           float32 `json:"PercentualAliquotaICMS"`
	BaseCalculoICMS                  float64 `json:"BaseCalculoICMS"`
	ValorICMS                        float64 `json:"ValorICMS"`
	ValorIsentoNaoTributado          float64 `json:"ValorIsentoNaoTributado"`
	ValorNaoConfiraDebitoCreditoICMS float64 `json:"ValorNaoConfiraDebitoCreditoICMS"`
}

func (r *RegistroTipo5) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroTipo5

	err = posicaoParaValor.ReturnByType(&r.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CFOP, "CFOP")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.PercentualAliquotaICMS, "PercentualAliquotaICMS")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.BaseCalculoICMS, "BaseCalculoICMS")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorICMS, "ValorICMS")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorIsentoNaoTributado, "ValorIsentoNaoTributado")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorNaoConfiraDebitoCreditoICMS, "ValorNaoConfiraDebitoCreditoICMS")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistroTipo5 = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":                    {0, 2, 0},
	"CFOP":                             {2, 6, 0},
	"PercentualAliquotaICMS":           {6, 10, 2},
	"BaseCalculoICMS":                  {10, 19, 2},
	"ValorICMS":                        {19, 28, 2},
	"ValorIsentoNaoTributado":          {28, 37, 2},
	"ValorNaoConfiraDebitoCreditoICMS": {37, 46, 2},
}
