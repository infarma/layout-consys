package arquivoDeNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroTipo4 struct {
	Identificador                         string  `json:"Identificador"`
	CodigoProdutoFornecedor               int64   `json:"CodigoProdutoFornecedor"`
	CodigoBarrasProduto                   int64   `json:"CodigoBarrasProduto"`
	QuantidadeFaturada                    int32   `json:"QuantidadeFaturada"`
	ValorUnitarioProduto                  float64 `json:"ValorUnitarioProduto"`
	ValorDescontoProduto                  float64 `json:"ValorDescontoProduto"`
	BaseCalculoICMS                       float64 `json:"BaseCalculoICMS"`
	BaseCalculoICMSSubstituicaoTributaria float64 `json:"BaseCalculoICMSSubstituicaoTributaria"`
	PercentualICMS                        float32 `json:"PercentualICMS"`
	PercentualIPI                         float32 `json:"PercentualIPI"`
	ValorICMS                             float64 `json:"ValorICMS"`
	ValorIPI                              float64 `json:"ValorIPI"`
	ValorICMSRepassado                    float64 `json:"ValorICMSRepassado"`
	CFOP                                  int32   `json:"CFOP"`
	CodigoSubstituicaoTibutaria           int32   `json:"CodigoSubstituicaoTibutaria"`
	IdentificadorLista                    string  `json:"IdentificadorLista"`
	NumeroOrdemImpressaoNotaFiscal        int32   `json:"NumeroOrdemImpressaoNotaFiscal"`
	PrazoPorProduto                       int32   `json:"PrazoPorProduto"`
	ValorRepasse                          float64 `json:"ValorRepasse"`
	ValorTotalProduto                     float64 `json:"ValorTotalProduto"`
	CSTProduto                            int32   `json:"CSTProduto"`
	ValorUnitarioDescontoFinanceiro       float64 `json:"ValorUnitarioDescontoFinanceiro"`
}

func (r *RegistroTipo4) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroTipo4

	err = posicaoParaValor.ReturnByType(&r.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoProdutoFornecedor, "CodigoProdutoFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoBarrasProduto, "CodigoBarrasProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QuantidadeFaturada, "QuantidadeFaturada")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorUnitarioProduto, "ValorUnitarioProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorDescontoProduto, "ValorDescontoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.BaseCalculoICMS, "BaseCalculoICMS")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.BaseCalculoICMSSubstituicaoTributaria, "BaseCalculoICMSSubstituicaoTributaria")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.PercentualICMS, "PercentualICMS")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.PercentualIPI, "PercentualIPI")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorICMS, "ValorICMS")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorIPI, "ValorIPI")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorICMSRepassado, "ValorICMSRepassado")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CFOP, "CFOP")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoSubstituicaoTibutaria, "CodigoSubstituicaoTibutaria")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.IdentificadorLista, "IdentificadorLista")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NumeroOrdemImpressaoNotaFiscal, "NumeroOrdemImpressaoNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.PrazoPorProduto, "PrazoPorProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorRepasse, "ValorRepasse")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorTotalProduto, "ValorTotalProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CSTProduto, "CSTProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorUnitarioDescontoFinanceiro, "ValorUnitarioDescontoFinanceiro")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistroTipo4 = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":                         {0, 2, 0},
	"CodigoProdutoFornecedor":               {2, 12, 0},
	"CodigoBarrasProduto":                   {12, 25, 0},
	"QuantidadeFaturada":                    {25, 30, 0},
	"ValorUnitarioProduto":                  {30, 39, 2},
	"ValorDescontoProduto":                  {39, 48, 2},
	"BaseCalculoICMS":                       {48, 57, 2},
	"BaseCalculoICMSSubstituicaoTributaria": {57, 66, 2},
	"PercentualICMS":                        {64, 70, 2},
	"PercentualIPI":                         {70, 74, 2},
	"ValorICMS":                             {74, 83, 2},
	"ValorIPI":                              {83, 92, 2},
	"ValorICMSRepassado":                    {92, 101, 2},
	"CFOP":                                  {101, 105, 0},
	"CodigoSubstituicaoTibutaria":           {105, 106, 0},
	"IdentificadorLista":                    {106, 107, 0},
	"NumeroOrdemImpressaoNotaFiscal":        {107, 111, 0},
	"PrazoPorProduto":                       {111, 114, 0},
	"ValorRepasse":                          {114, 123, 2},
	"ValorTotalProduto":                     {123, 132, 2},
	"CSTProduto":                            {132, 135, 0},
	"ValorUnitarioDescontoFinanceiro":       {135, 144, 2},
}
