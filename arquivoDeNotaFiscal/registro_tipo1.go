package arquivoDeNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroTipo1 struct {
	Identificador                   string `json:"Identificador"`
	CnpjFornecedor                  int64  `json:"CnpjFornecedor"`
	IncricaoEstadualFornecedor      string `json:"IncricaoEstadualFornecedor"`
	SiglaUnidadeFederacaoFornecedor string `json:"SiglaUnidadeFederacaoFornecedor"`
	CodigoPedidoCliente             string `json:"CodigoPedidoCliente"`
	DataEmissao                     int32  `json:"DataEmissao"`
	HoraEmissao                     int32  `json:"HoraEmissao"`
	DataSaidaNota                   int32  `json:"DataSaidaNota"`
	CnpjCliente                     int64  `json:"CnpjCliente"`
}

func (r *RegistroTipo1) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroTipo1

	err = posicaoParaValor.ReturnByType(&r.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CnpjFornecedor, "CnpjFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.IncricaoEstadualFornecedor, "IncricaoEstadualFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.SiglaUnidadeFederacaoFornecedor, "SiglaUnidadeFederacaoFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoPedidoCliente, "CodigoPedidoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.DataEmissao, "DataEmissao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.HoraEmissao, "HoraEmissao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.DataSaidaNota, "DataSaidaNota")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CnpjCliente, "CnpjCliente")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistroTipo1 = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":                   {0, 2, 0},
	"CnpjFornecedor":                  {2, 16, 0},
	"IncricaoEstadualFornecedor":      {16, 30, 0},
	"SiglaUnidadeFederacaoFornecedor": {30, 32, 0},
	"CodigoPedidoCliente":             {32, 42, 0},
	"DataEmissao":                     {42, 50, 0},
	"HoraEmissao":                     {50, 56, 0},
	"DataSaidaNota":                   {56, 64, 0},
	"CnpjCliente":                     {64, 78, 0},
}
