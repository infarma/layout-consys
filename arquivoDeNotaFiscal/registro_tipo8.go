package arquivoDeNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroTipo8 struct {
	Identificador                    string  `json:"Identificador"`
	NumeroIdentificacaoBloqueto      string  `json:"NumeroIdentificacaoBloqueto"`
	DataEmissao                      int32   `json:"DataEmissao"`
	DataVencimento                   int32   `json:"DataVencimento"`
	ValorParcela                     float64 `json:"ValorParcela"`
	NumeroNotaFiscalOriginouBloqueto int32   `json:"NumeroNotaFiscalOriginouBloqueto"`
}

func (r *RegistroTipo8) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroTipo8

	err = posicaoParaValor.ReturnByType(&r.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NumeroIdentificacaoBloqueto, "NumeroIdentificacaoBloqueto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.DataEmissao, "DataEmissao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.DataVencimento, "DataVencimento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorParcela, "ValorParcela")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NumeroNotaFiscalOriginouBloqueto, "NumeroNotaFiscalOriginouBloqueto")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistroTipo8 = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":                    {0, 2, 0},
	"NumeroIdentificacaoBloqueto":      {2, 32, 0},
	"DataEmissao":                      {32, 40, 0},
	"DataVencimento":                   {40, 48, 0},
	"ValorParcela":                     {48, 57, 2},
	"NumeroNotaFiscalOriginouBloqueto": {57, 63, 0},
}
