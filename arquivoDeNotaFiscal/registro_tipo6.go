package arquivoDeNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroTipo6 struct {
	Identificador      string  `json:"Identificador"`
	ValorTotalNota     float64 `json:"ValorTotalNota"`
	ValorTotalProdutos int64   `json:"ValorTotalProdutos"`
}

func (r *RegistroTipo6) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroTipo6

	err = posicaoParaValor.ReturnByType(&r.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorTotalNota, "ValorTotalNota")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorTotalProdutos, "ValorTotalProdutos")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistroTipo6 = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":      {0, 2, 0},
	"ValorTotalNota":     {2, 11, 2},
	"ValorTotalProdutos": {11, 20, 0},
}
