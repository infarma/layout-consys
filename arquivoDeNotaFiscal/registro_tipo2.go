package arquivoDeNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroTipo2 struct {
	Identificador                         string  `json:"Identificador"`
	NumeroNotaFiscal                      int32   `json:"NumeroNotaFiscal"`
	ModeloNotaFiscal                      string  `json:"ModeloNotaFiscal"`
	SerieSubSerieNotaFiscal               string  `json:"SerieSubSerieNotaFiscal"`
	BaseCalculoICMSSubstituicaoTributaria float64 `json:"BaseCalculoICMSSubstituicaoTributaria"`
	BaseCalculoICMS                       float64 `json:"BaseCalculoICMS"`
}

func (r *RegistroTipo2) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroTipo2

	err = posicaoParaValor.ReturnByType(&r.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NumeroNotaFiscal, "NumeroNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ModeloNotaFiscal, "ModeloNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.SerieSubSerieNotaFiscal, "SerieSubSerieNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.BaseCalculoICMSSubstituicaoTributaria, "BaseCalculoICMSSubstituicaoTributaria")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.BaseCalculoICMS, "BaseCalculoICMS")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistroTipo2 = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":                         {0, 2, 0},
	"NumeroNotaFiscal":                      {2, 8, 0},
	"ModeloNotaFiscal":                      {8, 11, 0},
	"SerieSubSerieNotaFiscal":               {11, 16, 0},
	"BaseCalculoICMSSubstituicaoTributaria": {16, 25, 2},
	"BaseCalculoICMS":                       {25, 32, 2},
}
