package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroTipo8 struct {
	Identificador      int32 `json:"Identificador"`
	NumeroItensPedidos int32 `json:"NumeroItensPedidos"`
}

func (r *RegistroTipo8) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroTipo8

	err = posicaoParaValor.ReturnByType(&r.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NumeroItensPedidos, "NumeroItensPedidos")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistroTipo8 = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":      {0, 1, 0},
	"NumeroItensPedidos": {1, 11, 0},
}
