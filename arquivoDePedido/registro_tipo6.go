package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroTipo6 struct {
	Identificador     int32 `json:"Identificador"`
	HoraGeracaoPedido int32 `json:"HoraGeracaoPedido"`
}

func (r *RegistroTipo6) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroTipo6

	err = posicaoParaValor.ReturnByType(&r.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.HoraGeracaoPedido, "HoraGeracaoPedido")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistroTipo6 = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":     {0, 1, 0},
	"HoraGeracaoPedido": {1, 5, 0},
}
