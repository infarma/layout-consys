package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroTipo2 struct {
	Identificador int32  `json:"Identificador"`
	CodigoPedido  string `json:"CodigoPedido"`
}

func (r *RegistroTipo2) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroTipo2

	err = posicaoParaValor.ReturnByType(&r.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoPedido, "CodigoPedido")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistroTipo2 = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador": {0, 1, 0},
	"CodigoPedido":  {1, 11, 0},
}
