package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroTipo3 struct {
	Identificador int32  `json:"Identificador"`
	CnpjCliente   string `json:"CnpjCliente"`
}

func (r *RegistroTipo3) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroTipo3

	err = posicaoParaValor.ReturnByType(&r.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CnpjCliente, "CnpjCliente")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistroTipo3 = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador": {0, 1, 0},
	"CnpjCliente":   {1, 15, 0},
}
