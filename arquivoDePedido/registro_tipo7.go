package arquivoDePedido

import (
	"bitbucket.org/infarma/gerador-layouts-posicoes"
	"strconv"
	"strings"
)

type RegistroTipo7 struct {
	Identificador           int32  `json:"Identificador"`
	CodigoProdutoFornecedor int64 `json:"CodigoProdutoFornecedor"`
	QuantidadeProduto       int32  `json:"QuantidadeProduto"`
	CodigoProdutoCliente    string `json:"CodigoProdutoCliente"`
}

func (r *RegistroTipo7) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroTipo7

	err = posicaoParaValor.ReturnByType(&r.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoProdutoFornecedor, "CodigoProdutoFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QuantidadeProduto, "QuantidadeProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoProdutoCliente, "CodigoProdutoCliente")
	if err != nil {
		return err
	}

	r.CodigoProdutoCliente = strings.TrimSpace(r.CodigoProdutoCliente)

	return err
}

var PosicoesRegistroTipo7 = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":           {0, 1, 0},
	"CodigoProdutoFornecedor": {1, 11, 0},
	"QuantidadeProduto":       {11, 16, 0},
	"CodigoProdutoCliente":    {16, 27, 0},
}

func ConvertStringToInt(valor string) int {
	v, _ := strconv.Atoi(valor)
	return v
}