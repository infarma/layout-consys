package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroTipo5 struct {
	Identificador     int32 `json:"Identificador"`
	DataGeracaoPedido int32 `json:"DataGeracaoPedido"`
}

func (r *RegistroTipo5) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroTipo5

	err = posicaoParaValor.ReturnByType(&r.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.DataGeracaoPedido, "DataGeracaoPedido")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistroTipo5 = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":     {0, 1, 0},
	"DataGeracaoPedido": {1, 9, 0},
}
