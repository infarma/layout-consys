package arquivoDePedido

import (
	"bitbucket.org/infarma/gerador-layouts-posicoes"
	"strings"
)

type RegistroTipo1 struct {
	Identificador   int32  `json:"Identificador"`
	CodigoCliente   string `json:"CodigoCliente"`
	TipoFaturamento string `json:"TipoFaturamento"`
}

func (r *RegistroTipo1) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroTipo1

	err = posicaoParaValor.ReturnByType(&r.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoCliente, "CodigoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.TipoFaturamento, "TipoFaturamento")
	if err != nil {
		return err
	}

	r.TipoFaturamento = strings.TrimSpace(r.TipoFaturamento)

	return err
}

var PosicoesRegistroTipo1 = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":   {0, 1, 0},
	"CodigoCliente":   {1, 9, 0},
	"TipoFaturamento": {9, 11, 0},
}
