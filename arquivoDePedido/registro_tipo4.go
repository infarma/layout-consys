package arquivoDePedido

import (
	"bitbucket.org/infarma/gerador-layouts-posicoes"
	"strings"
)

type RegistroTipo4 struct {
	Identificador int32  `json:"Identificador"`
	Reservado     string `json:"Reservado"`
}

func (r *RegistroTipo4) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroTipo4

	err = posicaoParaValor.ReturnByType(&r.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.Reservado, "Reservado")
	if err != nil {
		return err
	}

	r.Reservado = strings.TrimSpace(r.Reservado)
	return err
}

var PosicoesRegistroTipo4 = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador": {0, 1, 0},
	"Reservado":     {1, 2, 0},
}
