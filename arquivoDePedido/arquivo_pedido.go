package arquivoDePedido

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	RegistroTipo1 RegistroTipo1   `json:"RegistroTipo1"`
	RegistroTipo2 RegistroTipo2   `json:"RegistroTipo2"`
	RegistroTipo3 RegistroTipo3   `json:"RegistroTipo3"`
	RegistroTipo4 RegistroTipo4   `json:"RegistroTipo4"`
	RegistroTipo5 RegistroTipo5   `json:"RegistroTipo5"`
	RegistroTipo6 RegistroTipo6   `json:"RegistroTipo6"`
	RegistroTipo7 []RegistroTipo7 `json:"RegistroTipo7"`
	RegistroTipo8 RegistroTipo8   `json:"RegistroTipo8"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:1])

		if identificador == "1" {
			err = arquivo.RegistroTipo1.ComposeStruct(string(runes))
		} else if identificador == "2" {
			err = arquivo.RegistroTipo2.ComposeStruct(string(runes))
		} else if identificador == "3" {
			err = arquivo.RegistroTipo3.ComposeStruct(string(runes))
		} else if identificador == "4" {
			err = arquivo.RegistroTipo4.ComposeStruct(string(runes))
		} else if identificador == "5" {
			err = arquivo.RegistroTipo5.ComposeStruct(string(runes))
		} else if identificador == "6" {
			err = arquivo.RegistroTipo6.ComposeStruct(string(runes))
		} else if identificador == "7" {
			registro7 := RegistroTipo7{}
			registro7.ComposeStruct(string(runes))
			arquivo.RegistroTipo7 = append(arquivo.RegistroTipo7, registro7)
		} else if identificador == "8" {
			err = arquivo.RegistroTipo8.ComposeStruct(string(runes))
		}
	}
	return arquivo, err
}
