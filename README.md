## Arquivo de Pedido
gerador-layouts arquivoDePedido RegistroTipo1 Identificador:int32:0:1 CodigoCliente:string:1:9 TipoFaturamento:string:9:11

gerador-layouts arquivoDePedido RegistroTipo2 Identificador:int32:0:1 CodigoPedido:string:1:11

gerador-layouts arquivoDePedido RegistroTipo3 Identificador:int32:0:1 CnpjCliente:string:1:15

gerador-layouts arquivoDePedido RegistroTipo4 Identificador:int32:0:1 Reservado:string:1:2

gerador-layouts arquivoDePedido RegistroTipo5 Identificador:int32:0:1 DataGeracaoPedido:int32:1:9

gerador-layouts arquivoDePedido RegistroTipo6 Identificador:int32:0:1 HoraGeracaoPedido:int32:1:5

gerador-layouts arquivoDePedido RegistroTipo7 Identificador:int32:0:1 CodigoProdutoFornecedor:string:1:11 QuantidadeProduto:int32:11:16 CodigoProdutoCliente:string:16:27

gerador-layouts arquivoDePedido RegistroTipo8 Identificador:int32:0:1 NumeroItensPedidos:int32:1:11


## Arquivo de Nota Fiscal
gerador-layouts arquivoDeNotaFiscal RegistroTipo1 Identificador:string:0:2 CnpjFornecedor:int64:2:16 IncricaoEstadualFornecedor:string:16:30 SiglaUnidadeFederacaoFornecedor:string:30:32 CodigoPedidoCliente:string:32:42 DataEmissao:int32:42:50 HoraEmissao:int32:50:56 DataSaidaNota:int32:56:64 CnpjCliente:int64:64:78

gerador-layouts arquivoDeNotaFiscal RegistroTipo2 Identificador:string:0:2 NumeroNotaFiscal:int32:2:8 ModeloNotaFiscal:string:8:11 SerieSubSerieNotaFiscal:string:11:16 BaseCalculoICMSSubstituicaoTributaria:float64:16:25:2 BaseCalculoICMS:float64:25:32:2

gerador-layouts arquivoDeNotaFiscal RegistroTipo3 Identificador:string:0:2 ValorTotalProdutos:float64:2:11:2 ValorIPI:float64:11:20:2 ValorFrete:float64:20:29:2 ValorSeguro:float64:29:38:2 ValorOutrasDespesas:float64:38:47:2 ValorDescontoDadoTotalNotaFiscal:float64:47:56:2 ValorRepasseICMS:float64:56:65:2 ValorTotalNotaFiscal:float64:65:74:2 ValorSubstituicaoTributaria:float64:74:83:2

gerador-layouts arquivoDeNotaFiscal RegistroTipo4 Identificador:string:0:2 CodigoProdutoFornecedor:int64:2:12 CodigoBarrasProduto:int64:12:25 QuantidadeFaturada:int32:25:30 ValorUnitarioProduto:float64:30:39:2 ValorDescontoProduto:float64:39:48:2 BaseCalculoICMS:float64:48:57:2 BaseCalculoICMSSubstituicaoTributaria:float64:57:66:2 PercentualICMS:float32:64:70:2 PercentualIPI:float32:70:74:2 ValorICMS:float64:74:83:2 ValorIPI:float64:83:92:2 ValorICMSRepassado:float64:92:101:2 CFOP:int32:101:105 CodigoSubstituicaoTibutaria:int32:105:106 IdentificadorLista:string:106:107 NumeroOrdemImpressaoNotaFiscal:int32:107:111 PrazoPorProduto:int32:111:114 ValorRepasse:float64:114:123:2 ValorTotalProduto:float64:123:132:2 CSTProduto:int32:132:135 ValorUnitarioDescontoFinanceiro:float64:135:144:2

gerador-layouts arquivoDeNotaFiscal RegistroTipo5 Identificador:string:0:2 CFOP:int32:2:6 PercentualAliquotaICMS:float32:6:10:2 BaseCalculoICMS:float64:10:19:2 ValorICMS:float64:19:28:2 ValorIsentoNaoTributado:float64:28:37:2 ValorNaoConfiraDebitoCreditoICMS:float64:37:46:2

gerador-layouts arquivoDeNotaFiscal RegistroTipo6 Identificador:string:0:2 ValorTotalNota:float64:2:11:2 ValorTotalProdutos:int64:11:20:2

gerador-layouts arquivoDeNotaFiscal RegistroTipo8 Identificador:string:0:2 NumeroIdentificacaoBloqueto:string:2:32 DataEmissao:int32:32:40 DataVencimento:int32:40:48 ValorParcela:float64:48:57:2 NumeroNotaFiscalOriginouBloqueto:int32:57:63