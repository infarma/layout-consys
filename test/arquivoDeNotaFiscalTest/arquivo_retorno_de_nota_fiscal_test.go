package arquivoDeNotaFiscalTest

import (
	"layout-consys/arquivoDeNotaFiscal"
	"testing"
	"time"
)

func TestRegistro01(t *testing.T){
	cnpjFornecedor := "2000831000199"
	inscEstadualFornecedor := "200083"
	siglaUniFederacaoFornecedor := "CE"
	codPedidoCliente := "4521"

	str := "2017-05-08T21:04:52.001Z"
	dataEmissao, _ := time.Parse(time.RFC3339, str)

	str1 := "2017-05-10T21:04:52.001Z"
	dataSaidaNota, _ := time.Parse(time.RFC3339, str1)

	cnpjCliente := "2000831000199"

	registro01 := arquivoDeNotaFiscal.Registro01(cnpjFornecedor, inscEstadualFornecedor , siglaUniFederacaoFornecedor, codPedidoCliente,
		dataEmissao, dataSaidaNota, cnpjCliente)

	if (len(registro01) != 78)  {
		t.Error("Registro 01 não tem o tamanho adequado")
	}else{
		if registro01 != "0102000831000199200083        CE4521      080520172104521005201702000831000199" {
			t.Error("Registro 01 não é compativel")
		}
	}
}

func TestRegistro02(t *testing.T){
	numNotaFiscal := 234
	modeloDaNotaFiscal := "2"
	serieNotaFiscal := "1"
	baseICMSSubTri := 12.58
	baseICMS := 21.40

		registro02 := arquivoDeNotaFiscal.Registro02(int64(numNotaFiscal), modeloDaNotaFiscal, serieNotaFiscal, baseICMSSubTri, baseICMS)

		if (len(registro02) != 34)  {
			t.Error("Registro 02 não tem o tamanho adequado")
		}else{
		if registro02 != "020002342  1    000001258000002140" {
			t.Error("Registro 02 não é compativel")
		}
	}
}

func TestRegistro03(t *testing.T){
	valorTotalProdutos := 21.40
	valorIPI := 21.40
	valorFrete := 21.40
	valorSeguro := 21.40
	valorDespesas := 21.40
	valorDescontoTotal := 21.40
	valorRepasseICMS := 21.40
	valorTotalNotaFiscal := 21.40
	valorSubsTribuICMS := 21.40

	registro03 := arquivoDeNotaFiscal.Registro03(valorTotalProdutos, valorIPI, valorFrete, valorSeguro, valorDespesas, valorDescontoTotal,
																								valorRepasseICMS, valorTotalNotaFiscal, valorSubsTribuICMS)

	if (len(registro03) != 83)  {
		t.Error("Registro 03 não tem o tamanho adequado")
	}else{
		if registro03 != "03000002140000002140000002140000002140000002140000002140000002140000002140000002140" {
			t.Error("Registro 03 não é compativel")
		}
	}
}

func TestRegistro04(t *testing.T){
	codProdutoForn := 231677
	codBarrasProduto := 7896004715803
	qtdFatura := 100
	valorUnitario := 134.76
	valorDesconto := 21.40
	baseCalculoICMS := 21.40
	baseICMSSubTri := 21.40
	percentualICMS := 1.20
	percentualIPI := 1.20
	valorICMS := 21.40
	valorIPI := 21.40
	valorICMSRepassado := 21.40
	cFOP := 123
	codSituacaoTributaria := 1
	ideListaPositiva := "1"
	numOrdemImpressaoNotaFiscal := 123
	prazoPorProduto := 12
	valorRepasse := 21.40
	valorTotalProduto := 21.40
	cSTPorduto := 12
	valorUniDescontoFinanceiro := 21.40

	registro04 := arquivoDeNotaFiscal.Registro04(int64(codProdutoForn), int64(codBarrasProduto), int32(qtdFatura), valorUnitario, valorDesconto,
																								baseCalculoICMS, baseICMSSubTri, float32(percentualICMS), float32(percentualIPI), valorICMS, valorIPI,
																								valorICMSRepassado, int32(cFOP), int8(codSituacaoTributaria), ideListaPositiva,
																								int32(numOrdemImpressaoNotaFiscal), int32(prazoPorProduto), valorRepasse, valorTotalProduto,
																								int32(cSTPorduto), valorUniDescontoFinanceiro)

	if (len(registro04) != 144)  {
		t.Error("Registro 04 não tem o tamanho adequado")
	}else{
		if registro04 != "040000231677789600471580300100000013476000002140000002140000002140012001200000021400000021400000021400123110123012000002140000002140012000002140" {
			t.Error("Registro 04 não é compativel")
		}
	}
}

func TestRegistro05(t *testing.T){
	cFOP := 123
	aliquotaICMS := 1.20
	baseCalculoICMS := 21.40
	valorICMS := 21.40
	valorIsentoNaoTributado := 21.40
	valorNaoConfiraDebitoCreditoICMS := 21.40

	registro05 := arquivoDeNotaFiscal.Registro05(int32(cFOP), float32(aliquotaICMS), baseCalculoICMS, valorICMS, valorIsentoNaoTributado, valorNaoConfiraDebitoCreditoICMS)
//0501230120000002140000002140000002140000002140
	if (len(registro05) != 46)  {
		t.Error("Registro 05 não tem o tamanho adequado")
	}else{
		if registro05 != "0501230120000002140000002140000002140000002140" {
			t.Error("Registro 05 não é compativel")
		}
	}
}

func TestRegistro06(t *testing.T){
	valorTotalNota := 21.40
	valorTotalProduto := 21.40

	registro06 := arquivoDeNotaFiscal.Registro06(valorTotalNota, valorTotalProduto)

	if (len(registro06) != 20)  {
		t.Error("Registro 06 não tem o tamanho adequado")
	}else{
		if registro06 != "06000002140000002140" {
			t.Error("Registro 06 não é compativel")
		}
	}
}

func TestRegistro08(t *testing.T){
	numIdentificacaoBloqueto := "45"

	str := "2017-05-08T21:04:52.001Z"
	dataEmissao, _ := time.Parse(time.RFC3339, str)

	str1 := "2017-05-10T21:04:52.001Z"
	dataVencimento, _ := time.Parse(time.RFC3339, str1)

	valorParcela := 21.40
	numNotaFiscal := 234

	registro08 := arquivoDeNotaFiscal.Registro08(numIdentificacaoBloqueto, dataEmissao, dataVencimento, valorParcela, int64(numNotaFiscal))

	if (len(registro08) != 63)  {
		t.Error("Registro 08 não tem o tamanho adequado")
	}else{
		if registro08 != "0845                            0805201710052017000002140000234" {
			t.Error("Registro 08 não é compativel")
		}
	}
}