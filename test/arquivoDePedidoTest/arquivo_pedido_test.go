package arquivoDePedidoTest

import (
	"os"
	"testing"
	"layout-consys/arquivoDePedido"
)

func TestGetArquivoPedido(t *testing.T) {
	f, err := os.Open("../fileTest/LOJA01.ped")

	if err != nil {
		t.Error("Erro ao abrir o arquivo modelo")
	}

	arq, err := arquivoDePedido.GetStruct(f)

	if err != nil {
		t.Error("Erro ao ler o arquivo")
	}

	//Identificação do Cliente
	var registro01 arquivoDePedido.RegistroTipo1
	registro01.Identificador = 1
	registro01.CodigoCliente = "00000000"
	registro01.TipoFaturamento = ""

	if registro01.Identificador != arq.RegistroTipo1.Identificador {
		t.Error(" não é compativel")
	}
	if registro01.CodigoCliente != arq.RegistroTipo1.CodigoCliente {
		t.Error("Codigo do Cliente não é compativel")
	}
	if registro01.TipoFaturamento != arq.RegistroTipo1.TipoFaturamento {
		t.Error("Tipo Faturamento não é compativel")
	}

	//Identificação do Pedido
	var registro02 arquivoDePedido.RegistroTipo2
	registro02.Identificador = 2
	registro02.CodigoPedido = "S130661421"

	if registro02.Identificador != arq.RegistroTipo2.Identificador {
		t.Error("Identificador não é compativel")
	}
	if registro02.CodigoPedido != arq.RegistroTipo2.CodigoPedido {
		t.Error("Codigo do Pedido não é compativel")
	}

	//Reservado
	var registro03 arquivoDePedido.RegistroTipo3
	registro03.Identificador = 3
	registro03.CnpjCliente =  "03175297000114"

	if registro03.Identificador != arq.RegistroTipo3.Identificador {
		t.Error("Identificador não é compativel")
	}

	if registro03.CnpjCliente != arq.RegistroTipo3.CnpjCliente{
		t.Error("Cnpj Cliente não é compativel")
	}

	//Reservado
	var registro04 arquivoDePedido.RegistroTipo4
	registro04.Identificador = 4
	registro04.Reservado = ""

	if registro04.Identificador != arq.RegistroTipo4.Identificador{
		t.Error("Identificador não é compativel")
	}
	if registro04.Reservado != arq.RegistroTipo4.Reservado{
		t.Error("Reservado não é compativel")
	}

	//Data
	var registro05 arquivoDePedido.RegistroTipo5
	registro05.Identificador = 5
	registro05.DataGeracaoPedido = 20130618

	if registro05.Identificador != arq.RegistroTipo5.Identificador{
		t.Error("Identificador não é compativel")
	}
	if registro05.DataGeracaoPedido != arq.RegistroTipo5.DataGeracaoPedido{
		t.Error("Data Geracao Pedido não é compativel")
	}

	//Hora
	var registro06 arquivoDePedido.RegistroTipo6
	registro06.Identificador = 6
	registro06.HoraGeracaoPedido = 1153

	if registro06.Identificador != arq.RegistroTipo6.Identificador{
		t.Error("Identificador não é compativel")
	}
	if registro06.HoraGeracaoPedido != arq.RegistroTipo6.HoraGeracaoPedido{
		t.Error("Hora Geracao Pedido não é compativel")
	}

	//DadosDosProdutos
	var registro07 arquivoDePedido.RegistroTipo7
	registro07.Identificador = 7
	registro07.CodigoProdutoFornecedor = 23007
	registro07.QuantidadeProduto = 1
	registro07.CodigoProdutoCliente = "01194851"

	if registro07.Identificador != arq.RegistroTipo7[0].Identificador{
		t.Error("Identificador não é compativel")
	}
	if registro07.CodigoProdutoFornecedor != arq.RegistroTipo7[0].CodigoProdutoFornecedor{
		t.Error("Codigo Produto Fornecedor não é compativel")
	}
	if registro07.QuantidadeProduto != arq.RegistroTipo7[0].QuantidadeProduto{
		t.Error("Quantidade Produto não é compativel")
	}
	if registro07.CodigoProdutoCliente != arq.RegistroTipo7[0].CodigoProdutoCliente{
		t.Error("Codigo Produto Cliente não é compativel")
	}

	//FinalizadorDoPedido
	var registro08 arquivoDePedido.RegistroTipo8
	registro08.Identificador = 8
	registro08.NumeroItensPedidos = 97

	if registro08.Identificador != arq.RegistroTipo8.Identificador{
		t.Error("Identificador não é compativel")
	}
	if registro08.NumeroItensPedidos != arq.RegistroTipo8.NumeroItensPedidos{
		t.Error("Numero Itens Pedidos não é compativel")
	}
}